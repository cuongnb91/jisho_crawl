require 'nokogiri'
require 'open-uri'
class Word < ApplicationRecord
  has_and_belongs_to_many :sentences
  attr_accessor :jisho_crawl_doc
  attr_accessor :sentence_elements

  def jisho_crawl_doc
    uri = "http://jisho.org/search/#{URI.encode(word_value)}%20%23sentences"
    @jisho_crawl_doc ||= Nokogiri::HTML(open(uri))
  end

  def sentence_elements
    @sentence_elements ||= jisho_crawl_doc.xpath('//div[@class="sentence_content"]')
  end

  def analyze_sentence
    sentence_elements.each do |se|
      sentence = Sentence.create
      sentence_value = ""
      order = 1
      list_component_children = se.children[1].children
      list_component_children.each do |comp_child|
        if comp_child.class == Nokogiri::XML::Element
          comp_value = comp_child.children.last.children.to_s
          sentence_value += comp_value
          sentence.sentence_components.create sentence_component_order: order,
            sentence_component_value: comp_value
          order += 1
        elsif comp_child.class == Nokogiri::XML::Text
          if comp_child.to_s.strip != "" && comp_child.to_s.strip != "。"
            comp_value = comp_child.to_s.strip
            sentence_value += comp_value
            sentence.sentence_components.create sentence_component_order: order,
              sentence_component_value: comp_value
            order += 1
          end
        end
      end
      puts sentence_value
      sentence.update_attributes sentence_value: sentence_value
    end
  end
end

class Sentence < ApplicationRecord
  has_many :sentence_components, dependent: :destroy

  scope :has_word, ->word do
    where "sentence_value like '%#{word}%'"
  end
end

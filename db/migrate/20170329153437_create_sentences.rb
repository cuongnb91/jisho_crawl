class CreateSentences < ActiveRecord::Migration[5.0]
  def change
    create_table :sentences do |t|

      t.timestamps
      t.string :sentence_value
      t.string :level_id
    end
  end
end

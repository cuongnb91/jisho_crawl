class CreateSentenceComponents < ActiveRecord::Migration[5.0]
  def change
    create_table :sentence_components do |t|

      t.timestamps
      t.integer :sentence_id
      t.integer :sentence_component_order
      t.string  :sentence_component_value
    end
  end
end

class CreateSentencesWords < ActiveRecord::Migration[5.0]
  def change
    create_table :sentences_words do |t|

      t.timestamps
      t.integer :sentence_id
      t.integer :word_id
    end
  end
end

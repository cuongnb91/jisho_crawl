class CreateWords < ActiveRecord::Migration[5.0]
  def change
    create_table :words do |t|

      t.timestamps
      t.string :word_value
      t.string :reading
      t.string :meaning
      t.integer :level_id
    end
  end
end

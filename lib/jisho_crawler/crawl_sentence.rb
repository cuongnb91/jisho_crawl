require 'nokogiri'
require 'open-uri'

module CrawlSentence
  class << self
    def jisho_crawl_doc word_value
      uri = "http://jisho.org/search/#{URI.encode(word_value)}%20%23sentences"
      Nokogiri::HTML(open(uri))
    end

    def sentence_elements word_value
      jisho_crawl_doc(word_value).xpath('//div[@class="sentence_content"]')
    end

    def analyze_sentence word_value
      sentence_elements(word_value).each do |se|
        sentence = Sentence.create
        sentence_value = ""
        order = 1
        list_component_children = se.children[1].children
        list_component_children.each do |comp_child|
          if comp_child.class == Nokogiri::XML::Element
            comp_value = comp_child.children.last.children.to_s
            sentence_value += comp_value
            sentence.sentence_components.create sentence_component_order: order,
              sentence_component_value: comp_value
            order += 1
          elsif comp_child.class == Nokogiri::XML::Text
            if comp_child.to_s.strip != "" && comp_child.to_s.strip != "。"
              comp_value = comp_child.to_s.strip
              sentence_value += comp_value
              sentence.sentence_components.create sentence_component_order: order,
                sentence_component_value: comp_value
              order += 1
            end
          end
        end
        puts sentence_value
        sentence.update_attributes sentence_value: sentence_value
      end
    end
  end
end
